<?php



/**
 * Skeleton subclass for representing a row from the 'versichertes_objekt' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.propel
 */
class VersichertesObjekt extends BaseVersichertesObjekt
{
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME) {
        if ($name == 'beitrag') ; // do nothing
        else parent::setByName($name, $value, $type);
    }
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME) {
        if ($name == 'beitrag') {
            $btrs = $this->getBeitrags();
            $btrids = array();
            foreach ($btrs as $btr) $btrids[] = $btr->getId();
            return $btrids;
        }
        else return parent::getByName($name, $type);
    }
    public function getZahlweise() {
        return new Zahlweise(parent::getZahlweise());
    }
    public function setZahlweise($v) {
        parent::setZahlweise($v->getValue());
    }
    public function save(PropelPDO $con = null)
    {
        if ($this->isNew() || $this->mod >= 999999) {
            $this->setMod(0);
        } else {
            $this->setMod($this->mod + 1);
        }
        return parent::save($con);
    }
    
}
