<?php



/**
 * This class defines the structure of the 'stamm' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.propel.map
 */
class StammTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'propel.map.StammTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('stamm');
        $this->setPhpName('Stamm');
        $this->setClassname('Stamm');
        $this->setPackage('propel');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('mod', 'Mod', 'INTEGER', true, null, null);
        $this->addColumn('nachname', 'Nachname', 'VARCHAR', true, 100, null);
        $this->addColumn('vorname', 'Vorname', 'VARCHAR', true, 100, null);
        $this->addColumn('stasse', 'Stasse', 'VARCHAR', true, 100, null);
        $this->addColumn('plz', 'Plz', 'CHAR', true, 10, null);
        $this->addColumn('ort', 'Ort', 'VARCHAR', true, 100, null);
        $this->addColumn('zahlweise', 'Zahlweise', 'INTEGER', true, null, null);
        $this->addColumn('iban', 'Iban', 'CHAR', true, 20, null);
        $this->addColumn('bic', 'Bic', 'CHAR', true, 10, null);
        $this->addColumn('kommentar', 'Kommentar', 'VARCHAR', true, 1000, null);
        $this->addColumn('kundigung', 'Kundigung', 'INTEGER', true, null, 0);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('VersichertesObjekt', 'VersichertesObjekt', RelationMap::ONE_TO_MANY, array('id' => 'stamm_id', ), 'CASCADE', null, 'VersichertesObjekts');
    } // buildRelations()

} // StammTableMap
