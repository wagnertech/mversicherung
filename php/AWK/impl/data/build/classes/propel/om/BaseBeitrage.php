<?php


/**
 * Base class that represents a row from the 'beitrage' table.
 *
 *
 *
 * @package    propel.generator.propel.om
 */
abstract class BaseBeitrage extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'BeitragePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        BeitragePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the mod field.
     * @var        int
     */
    protected $mod;

    /**
     * The value for the jahr field.
     * @var        int
     */
    protected $jahr;

    /**
     * The value for the zahlweise field.
     * @var        int
     */
    protected $zahlweise;

    /**
     * The value for the betrag field.
     * @var        double
     */
    protected $betrag;

    /**
     * The value for the kommentar field.
     * @var        string
     */
    protected $kommentar;

    /**
     * The value for the vo_id field.
     * @var        int
     */
    protected $vo_id;

    /**
     * @var        VersichertesObjekt
     */
    protected $aVersichertesObjekt;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [mod] column value.
     *
     * @return int
     */
    public function getMod()
    {

        return $this->mod;
    }

    /**
     * Get the [jahr] column value.
     *
     * @return int
     */
    public function getJahr()
    {

        return $this->jahr;
    }

    /**
     * Get the [zahlweise] column value.
     *
     * @return int
     */
    public function getZahlweise()
    {

        return $this->zahlweise;
    }

    /**
     * Get the [betrag] column value.
     *
     * @return double
     */
    public function getBetrag()
    {

        return $this->betrag;
    }

    /**
     * Get the [kommentar] column value.
     *
     * @return string
     */
    public function getKommentar()
    {

        return $this->kommentar;
    }

    /**
     * Get the [vo_id] column value.
     *
     * @return int
     */
    public function getVoId()
    {

        return $this->vo_id;
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return Beitrage The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = BeitragePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [mod] column.
     *
     * @param  int $v new value
     * @return Beitrage The current object (for fluent API support)
     */
    public function setMod($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->mod !== $v) {
            $this->mod = $v;
            $this->modifiedColumns[] = BeitragePeer::MOD;
        }


        return $this;
    } // setMod()

    /**
     * Set the value of [jahr] column.
     *
     * @param  int $v new value
     * @return Beitrage The current object (for fluent API support)
     */
    public function setJahr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->jahr !== $v) {
            $this->jahr = $v;
            $this->modifiedColumns[] = BeitragePeer::JAHR;
        }


        return $this;
    } // setJahr()

    /**
     * Set the value of [zahlweise] column.
     *
     * @param  int $v new value
     * @return Beitrage The current object (for fluent API support)
     */
    public function setZahlweise($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->zahlweise !== $v) {
            $this->zahlweise = $v;
            $this->modifiedColumns[] = BeitragePeer::ZAHLWEISE;
        }


        return $this;
    } // setZahlweise()

    /**
     * Set the value of [betrag] column.
     *
     * @param  double $v new value
     * @return Beitrage The current object (for fluent API support)
     */
    public function setBetrag($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->betrag !== $v) {
            $this->betrag = $v;
            $this->modifiedColumns[] = BeitragePeer::BETRAG;
        }


        return $this;
    } // setBetrag()

    /**
     * Set the value of [kommentar] column.
     *
     * @param  string $v new value
     * @return Beitrage The current object (for fluent API support)
     */
    public function setKommentar($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->kommentar !== $v) {
            $this->kommentar = $v;
            $this->modifiedColumns[] = BeitragePeer::KOMMENTAR;
        }


        return $this;
    } // setKommentar()

    /**
     * Set the value of [vo_id] column.
     *
     * @param  int $v new value
     * @return Beitrage The current object (for fluent API support)
     */
    public function setVoId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->vo_id !== $v) {
            $this->vo_id = $v;
            $this->modifiedColumns[] = BeitragePeer::VO_ID;
        }

        if ($this->aVersichertesObjekt !== null && $this->aVersichertesObjekt->getId() !== $v) {
            $this->aVersichertesObjekt = null;
        }


        return $this;
    } // setVoId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->mod = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->jahr = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->zahlweise = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->betrag = ($row[$startcol + 4] !== null) ? (double) $row[$startcol + 4] : null;
            $this->kommentar = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->vo_id = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 7; // 7 = BeitragePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Beitrage object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aVersichertesObjekt !== null && $this->vo_id !== $this->aVersichertesObjekt->getId()) {
            $this->aVersichertesObjekt = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BeitragePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = BeitragePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aVersichertesObjekt = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BeitragePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = BeitrageQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(BeitragePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                BeitragePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aVersichertesObjekt !== null) {
                if ($this->aVersichertesObjekt->isModified() || $this->aVersichertesObjekt->isNew()) {
                    $affectedRows += $this->aVersichertesObjekt->save($con);
                }
                $this->setVersichertesObjekt($this->aVersichertesObjekt);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = BeitragePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . BeitragePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(BeitragePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(BeitragePeer::MOD)) {
            $modifiedColumns[':p' . $index++]  = '`mod`';
        }
        if ($this->isColumnModified(BeitragePeer::JAHR)) {
            $modifiedColumns[':p' . $index++]  = '`jahr`';
        }
        if ($this->isColumnModified(BeitragePeer::ZAHLWEISE)) {
            $modifiedColumns[':p' . $index++]  = '`zahlweise`';
        }
        if ($this->isColumnModified(BeitragePeer::BETRAG)) {
            $modifiedColumns[':p' . $index++]  = '`betrag`';
        }
        if ($this->isColumnModified(BeitragePeer::KOMMENTAR)) {
            $modifiedColumns[':p' . $index++]  = '`kommentar`';
        }
        if ($this->isColumnModified(BeitragePeer::VO_ID)) {
            $modifiedColumns[':p' . $index++]  = '`vo_id`';
        }

        $sql = sprintf(
            'INSERT INTO `beitrage` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`mod`':
                        $stmt->bindValue($identifier, $this->mod, PDO::PARAM_INT);
                        break;
                    case '`jahr`':
                        $stmt->bindValue($identifier, $this->jahr, PDO::PARAM_INT);
                        break;
                    case '`zahlweise`':
                        $stmt->bindValue($identifier, $this->zahlweise, PDO::PARAM_INT);
                        break;
                    case '`betrag`':
                        $stmt->bindValue($identifier, $this->betrag, PDO::PARAM_STR);
                        break;
                    case '`kommentar`':
                        $stmt->bindValue($identifier, $this->kommentar, PDO::PARAM_STR);
                        break;
                    case '`vo_id`':
                        $stmt->bindValue($identifier, $this->vo_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aVersichertesObjekt !== null) {
                if (!$this->aVersichertesObjekt->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aVersichertesObjekt->getValidationFailures());
                }
            }


            if (($retval = BeitragePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = BeitragePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getMod();
                break;
            case 2:
                return $this->getJahr();
                break;
            case 3:
                return $this->getZahlweise();
                break;
            case 4:
                return $this->getBetrag();
                break;
            case 5:
                return $this->getKommentar();
                break;
            case 6:
                return $this->getVoId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Beitrage'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Beitrage'][$this->getPrimaryKey()] = true;
        $keys = BeitragePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getMod(),
            $keys[2] => $this->getJahr(),
            $keys[3] => $this->getZahlweise(),
            $keys[4] => $this->getBetrag(),
            $keys[5] => $this->getKommentar(),
            $keys[6] => $this->getVoId(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aVersichertesObjekt) {
                $result['VersichertesObjekt'] = $this->aVersichertesObjekt->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = BeitragePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setMod($value);
                break;
            case 2:
                $this->setJahr($value);
                break;
            case 3:
                $this->setZahlweise($value);
                break;
            case 4:
                $this->setBetrag($value);
                break;
            case 5:
                $this->setKommentar($value);
                break;
            case 6:
                $this->setVoId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = BeitragePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setMod($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setJahr($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setZahlweise($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setBetrag($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setKommentar($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setVoId($arr[$keys[6]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(BeitragePeer::DATABASE_NAME);

        if ($this->isColumnModified(BeitragePeer::ID)) $criteria->add(BeitragePeer::ID, $this->id);
        if ($this->isColumnModified(BeitragePeer::MOD)) $criteria->add(BeitragePeer::MOD, $this->mod);
        if ($this->isColumnModified(BeitragePeer::JAHR)) $criteria->add(BeitragePeer::JAHR, $this->jahr);
        if ($this->isColumnModified(BeitragePeer::ZAHLWEISE)) $criteria->add(BeitragePeer::ZAHLWEISE, $this->zahlweise);
        if ($this->isColumnModified(BeitragePeer::BETRAG)) $criteria->add(BeitragePeer::BETRAG, $this->betrag);
        if ($this->isColumnModified(BeitragePeer::KOMMENTAR)) $criteria->add(BeitragePeer::KOMMENTAR, $this->kommentar);
        if ($this->isColumnModified(BeitragePeer::VO_ID)) $criteria->add(BeitragePeer::VO_ID, $this->vo_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(BeitragePeer::DATABASE_NAME);
        $criteria->add(BeitragePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Beitrage (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setMod($this->getMod());
        $copyObj->setJahr($this->getJahr());
        $copyObj->setZahlweise($this->getZahlweise());
        $copyObj->setBetrag($this->getBetrag());
        $copyObj->setKommentar($this->getKommentar());
        $copyObj->setVoId($this->getVoId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Beitrage Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return BeitragePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new BeitragePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a VersichertesObjekt object.
     *
     * @param                  VersichertesObjekt $v
     * @return Beitrage The current object (for fluent API support)
     * @throws PropelException
     */
    public function setVersichertesObjekt(VersichertesObjekt $v = null)
    {
        if ($v === null) {
            $this->setVoId(NULL);
        } else {
            $this->setVoId($v->getId());
        }

        $this->aVersichertesObjekt = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the VersichertesObjekt object, it will not be re-added.
        if ($v !== null) {
            $v->addBeitrag($this);
        }


        return $this;
    }


    /**
     * Get the associated VersichertesObjekt object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return VersichertesObjekt The associated VersichertesObjekt object.
     * @throws PropelException
     */
    public function getVersichertesObjekt(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aVersichertesObjekt === null && ($this->vo_id !== null) && $doQuery) {
            $this->aVersichertesObjekt = VersichertesObjektQuery::create()->findPk($this->vo_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aVersichertesObjekt->addBeitrags($this);
             */
        }

        return $this->aVersichertesObjekt;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->mod = null;
        $this->jahr = null;
        $this->zahlweise = null;
        $this->betrag = null;
        $this->kommentar = null;
        $this->vo_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aVersichertesObjekt instanceof Persistent) {
              $this->aVersichertesObjekt->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aVersichertesObjekt = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(BeitragePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
