<?php


/**
 * Base class that represents a row from the 'versichertes_objekt' table.
 *
 *
 *
 * @package    propel.generator.propel.om
 */
abstract class BaseVersichertesObjekt extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'VersichertesObjektPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        VersichertesObjektPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the mod field.
     * @var        int
     */
    protected $mod;

    /**
     * The value for the versicherungsnummer field.
     * @var        int
     */
    protected $versicherungsnummer;

    /**
     * The value for the beschreibung field.
     * @var        string
     */
    protected $beschreibung;

    /**
     * The value for the kommentar field.
     * @var        string
     */
    protected $kommentar;

    /**
     * The value for the stamm_id field.
     * @var        int
     */
    protected $stamm_id;

    /**
     * The value for the basispramie field.
     * @var        double
     */
    protected $basispramie;

    /**
     * The value for the zahlweise field.
     * @var        int
     */
    protected $zahlweise;

    /**
     * The value for the iban field.
     * @var        string
     */
    protected $iban;

    /**
     * The value for the bic field.
     * @var        string
     */
    protected $bic;

    /**
     * @var        Stamm
     */
    protected $aStamm;

    /**
     * @var        PropelObjectCollection|Beitrage[] Collection to store aggregation of Beitrage objects.
     */
    protected $collBeitrags;
    protected $collBeitragsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $beitragsScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [mod] column value.
     *
     * @return int
     */
    public function getMod()
    {

        return $this->mod;
    }

    /**
     * Get the [versicherungsnummer] column value.
     *
     * @return int
     */
    public function getVersicherungsnummer()
    {

        return $this->versicherungsnummer;
    }

    /**
     * Get the [beschreibung] column value.
     *
     * @return string
     */
    public function getBeschreibung()
    {

        return $this->beschreibung;
    }

    /**
     * Get the [kommentar] column value.
     *
     * @return string
     */
    public function getKommentar()
    {

        return $this->kommentar;
    }

    /**
     * Get the [stamm_id] column value.
     *
     * @return int
     */
    public function getStammId()
    {

        return $this->stamm_id;
    }

    /**
     * Get the [basispramie] column value.
     *
     * @return double
     */
    public function getBasispramie()
    {

        return $this->basispramie;
    }

    /**
     * Get the [zahlweise] column value.
     *
     * @return int
     */
    public function getZahlweise()
    {

        return $this->zahlweise;
    }

    /**
     * Get the [iban] column value.
     *
     * @return string
     */
    public function getIban()
    {

        return $this->iban;
    }

    /**
     * Get the [bic] column value.
     *
     * @return string
     */
    public function getBic()
    {

        return $this->bic;
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return VersichertesObjekt The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = VersichertesObjektPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [mod] column.
     *
     * @param  int $v new value
     * @return VersichertesObjekt The current object (for fluent API support)
     */
    public function setMod($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->mod !== $v) {
            $this->mod = $v;
            $this->modifiedColumns[] = VersichertesObjektPeer::MOD;
        }


        return $this;
    } // setMod()

    /**
     * Set the value of [versicherungsnummer] column.
     *
     * @param  int $v new value
     * @return VersichertesObjekt The current object (for fluent API support)
     */
    public function setVersicherungsnummer($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->versicherungsnummer !== $v) {
            $this->versicherungsnummer = $v;
            $this->modifiedColumns[] = VersichertesObjektPeer::VERSICHERUNGSNUMMER;
        }


        return $this;
    } // setVersicherungsnummer()

    /**
     * Set the value of [beschreibung] column.
     *
     * @param  string $v new value
     * @return VersichertesObjekt The current object (for fluent API support)
     */
    public function setBeschreibung($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->beschreibung !== $v) {
            $this->beschreibung = $v;
            $this->modifiedColumns[] = VersichertesObjektPeer::BESCHREIBUNG;
        }


        return $this;
    } // setBeschreibung()

    /**
     * Set the value of [kommentar] column.
     *
     * @param  string $v new value
     * @return VersichertesObjekt The current object (for fluent API support)
     */
    public function setKommentar($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->kommentar !== $v) {
            $this->kommentar = $v;
            $this->modifiedColumns[] = VersichertesObjektPeer::KOMMENTAR;
        }


        return $this;
    } // setKommentar()

    /**
     * Set the value of [stamm_id] column.
     *
     * @param  int $v new value
     * @return VersichertesObjekt The current object (for fluent API support)
     */
    public function setStammId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->stamm_id !== $v) {
            $this->stamm_id = $v;
            $this->modifiedColumns[] = VersichertesObjektPeer::STAMM_ID;
        }

        if ($this->aStamm !== null && $this->aStamm->getId() !== $v) {
            $this->aStamm = null;
        }


        return $this;
    } // setStammId()

    /**
     * Set the value of [basispramie] column.
     *
     * @param  double $v new value
     * @return VersichertesObjekt The current object (for fluent API support)
     */
    public function setBasispramie($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->basispramie !== $v) {
            $this->basispramie = $v;
            $this->modifiedColumns[] = VersichertesObjektPeer::BASISPRAMIE;
        }


        return $this;
    } // setBasispramie()

    /**
     * Set the value of [zahlweise] column.
     *
     * @param  int $v new value
     * @return VersichertesObjekt The current object (for fluent API support)
     */
    public function setZahlweise($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->zahlweise !== $v) {
            $this->zahlweise = $v;
            $this->modifiedColumns[] = VersichertesObjektPeer::ZAHLWEISE;
        }


        return $this;
    } // setZahlweise()

    /**
     * Set the value of [iban] column.
     *
     * @param  string $v new value
     * @return VersichertesObjekt The current object (for fluent API support)
     */
    public function setIban($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->iban !== $v) {
            $this->iban = $v;
            $this->modifiedColumns[] = VersichertesObjektPeer::IBAN;
        }


        return $this;
    } // setIban()

    /**
     * Set the value of [bic] column.
     *
     * @param  string $v new value
     * @return VersichertesObjekt The current object (for fluent API support)
     */
    public function setBic($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->bic !== $v) {
            $this->bic = $v;
            $this->modifiedColumns[] = VersichertesObjektPeer::BIC;
        }


        return $this;
    } // setBic()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->mod = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->versicherungsnummer = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->beschreibung = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->kommentar = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->stamm_id = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->basispramie = ($row[$startcol + 6] !== null) ? (double) $row[$startcol + 6] : null;
            $this->zahlweise = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->iban = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->bic = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 10; // 10 = VersichertesObjektPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating VersichertesObjekt object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aStamm !== null && $this->stamm_id !== $this->aStamm->getId()) {
            $this->aStamm = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(VersichertesObjektPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = VersichertesObjektPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aStamm = null;
            $this->collBeitrags = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(VersichertesObjektPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = VersichertesObjektQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(VersichertesObjektPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                VersichertesObjektPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aStamm !== null) {
                if ($this->aStamm->isModified() || $this->aStamm->isNew()) {
                    $affectedRows += $this->aStamm->save($con);
                }
                $this->setStamm($this->aStamm);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->beitragsScheduledForDeletion !== null) {
                if (!$this->beitragsScheduledForDeletion->isEmpty()) {
                    BeitrageQuery::create()
                        ->filterByPrimaryKeys($this->beitragsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->beitragsScheduledForDeletion = null;
                }
            }

            if ($this->collBeitrags !== null) {
                foreach ($this->collBeitrags as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = VersichertesObjektPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . VersichertesObjektPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(VersichertesObjektPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(VersichertesObjektPeer::MOD)) {
            $modifiedColumns[':p' . $index++]  = '`mod`';
        }
        if ($this->isColumnModified(VersichertesObjektPeer::VERSICHERUNGSNUMMER)) {
            $modifiedColumns[':p' . $index++]  = '`versicherungsnummer`';
        }
        if ($this->isColumnModified(VersichertesObjektPeer::BESCHREIBUNG)) {
            $modifiedColumns[':p' . $index++]  = '`beschreibung`';
        }
        if ($this->isColumnModified(VersichertesObjektPeer::KOMMENTAR)) {
            $modifiedColumns[':p' . $index++]  = '`kommentar`';
        }
        if ($this->isColumnModified(VersichertesObjektPeer::STAMM_ID)) {
            $modifiedColumns[':p' . $index++]  = '`stamm_id`';
        }
        if ($this->isColumnModified(VersichertesObjektPeer::BASISPRAMIE)) {
            $modifiedColumns[':p' . $index++]  = '`basispramie`';
        }
        if ($this->isColumnModified(VersichertesObjektPeer::ZAHLWEISE)) {
            $modifiedColumns[':p' . $index++]  = '`zahlweise`';
        }
        if ($this->isColumnModified(VersichertesObjektPeer::IBAN)) {
            $modifiedColumns[':p' . $index++]  = '`iban`';
        }
        if ($this->isColumnModified(VersichertesObjektPeer::BIC)) {
            $modifiedColumns[':p' . $index++]  = '`bic`';
        }

        $sql = sprintf(
            'INSERT INTO `versichertes_objekt` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`mod`':
                        $stmt->bindValue($identifier, $this->mod, PDO::PARAM_INT);
                        break;
                    case '`versicherungsnummer`':
                        $stmt->bindValue($identifier, $this->versicherungsnummer, PDO::PARAM_INT);
                        break;
                    case '`beschreibung`':
                        $stmt->bindValue($identifier, $this->beschreibung, PDO::PARAM_STR);
                        break;
                    case '`kommentar`':
                        $stmt->bindValue($identifier, $this->kommentar, PDO::PARAM_STR);
                        break;
                    case '`stamm_id`':
                        $stmt->bindValue($identifier, $this->stamm_id, PDO::PARAM_INT);
                        break;
                    case '`basispramie`':
                        $stmt->bindValue($identifier, $this->basispramie, PDO::PARAM_STR);
                        break;
                    case '`zahlweise`':
                        $stmt->bindValue($identifier, $this->zahlweise, PDO::PARAM_INT);
                        break;
                    case '`iban`':
                        $stmt->bindValue($identifier, $this->iban, PDO::PARAM_STR);
                        break;
                    case '`bic`':
                        $stmt->bindValue($identifier, $this->bic, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aStamm !== null) {
                if (!$this->aStamm->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aStamm->getValidationFailures());
                }
            }


            if (($retval = VersichertesObjektPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collBeitrags !== null) {
                    foreach ($this->collBeitrags as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = VersichertesObjektPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getMod();
                break;
            case 2:
                return $this->getVersicherungsnummer();
                break;
            case 3:
                return $this->getBeschreibung();
                break;
            case 4:
                return $this->getKommentar();
                break;
            case 5:
                return $this->getStammId();
                break;
            case 6:
                return $this->getBasispramie();
                break;
            case 7:
                return $this->getZahlweise();
                break;
            case 8:
                return $this->getIban();
                break;
            case 9:
                return $this->getBic();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['VersichertesObjekt'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['VersichertesObjekt'][$this->getPrimaryKey()] = true;
        $keys = VersichertesObjektPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getMod(),
            $keys[2] => $this->getVersicherungsnummer(),
            $keys[3] => $this->getBeschreibung(),
            $keys[4] => $this->getKommentar(),
            $keys[5] => $this->getStammId(),
            $keys[6] => $this->getBasispramie(),
            $keys[7] => $this->getZahlweise(),
            $keys[8] => $this->getIban(),
            $keys[9] => $this->getBic(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aStamm) {
                $result['Stamm'] = $this->aStamm->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collBeitrags) {
                $result['Beitrags'] = $this->collBeitrags->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = VersichertesObjektPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setMod($value);
                break;
            case 2:
                $this->setVersicherungsnummer($value);
                break;
            case 3:
                $this->setBeschreibung($value);
                break;
            case 4:
                $this->setKommentar($value);
                break;
            case 5:
                $this->setStammId($value);
                break;
            case 6:
                $this->setBasispramie($value);
                break;
            case 7:
                $this->setZahlweise($value);
                break;
            case 8:
                $this->setIban($value);
                break;
            case 9:
                $this->setBic($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = VersichertesObjektPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setMod($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setVersicherungsnummer($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setBeschreibung($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setKommentar($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setStammId($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setBasispramie($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setZahlweise($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setIban($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setBic($arr[$keys[9]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(VersichertesObjektPeer::DATABASE_NAME);

        if ($this->isColumnModified(VersichertesObjektPeer::ID)) $criteria->add(VersichertesObjektPeer::ID, $this->id);
        if ($this->isColumnModified(VersichertesObjektPeer::MOD)) $criteria->add(VersichertesObjektPeer::MOD, $this->mod);
        if ($this->isColumnModified(VersichertesObjektPeer::VERSICHERUNGSNUMMER)) $criteria->add(VersichertesObjektPeer::VERSICHERUNGSNUMMER, $this->versicherungsnummer);
        if ($this->isColumnModified(VersichertesObjektPeer::BESCHREIBUNG)) $criteria->add(VersichertesObjektPeer::BESCHREIBUNG, $this->beschreibung);
        if ($this->isColumnModified(VersichertesObjektPeer::KOMMENTAR)) $criteria->add(VersichertesObjektPeer::KOMMENTAR, $this->kommentar);
        if ($this->isColumnModified(VersichertesObjektPeer::STAMM_ID)) $criteria->add(VersichertesObjektPeer::STAMM_ID, $this->stamm_id);
        if ($this->isColumnModified(VersichertesObjektPeer::BASISPRAMIE)) $criteria->add(VersichertesObjektPeer::BASISPRAMIE, $this->basispramie);
        if ($this->isColumnModified(VersichertesObjektPeer::ZAHLWEISE)) $criteria->add(VersichertesObjektPeer::ZAHLWEISE, $this->zahlweise);
        if ($this->isColumnModified(VersichertesObjektPeer::IBAN)) $criteria->add(VersichertesObjektPeer::IBAN, $this->iban);
        if ($this->isColumnModified(VersichertesObjektPeer::BIC)) $criteria->add(VersichertesObjektPeer::BIC, $this->bic);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(VersichertesObjektPeer::DATABASE_NAME);
        $criteria->add(VersichertesObjektPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of VersichertesObjekt (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setMod($this->getMod());
        $copyObj->setVersicherungsnummer($this->getVersicherungsnummer());
        $copyObj->setBeschreibung($this->getBeschreibung());
        $copyObj->setKommentar($this->getKommentar());
        $copyObj->setStammId($this->getStammId());
        $copyObj->setBasispramie($this->getBasispramie());
        $copyObj->setZahlweise($this->getZahlweise());
        $copyObj->setIban($this->getIban());
        $copyObj->setBic($this->getBic());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getBeitrags() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBeitrag($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return VersichertesObjekt Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return VersichertesObjektPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new VersichertesObjektPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Stamm object.
     *
     * @param                  Stamm $v
     * @return VersichertesObjekt The current object (for fluent API support)
     * @throws PropelException
     */
    public function setStamm(Stamm $v = null)
    {
        if ($v === null) {
            $this->setStammId(NULL);
        } else {
            $this->setStammId($v->getId());
        }

        $this->aStamm = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Stamm object, it will not be re-added.
        if ($v !== null) {
            $v->addVersichertesObjekt($this);
        }


        return $this;
    }


    /**
     * Get the associated Stamm object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Stamm The associated Stamm object.
     * @throws PropelException
     */
    public function getStamm(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aStamm === null && ($this->stamm_id !== null) && $doQuery) {
            $this->aStamm = StammQuery::create()->findPk($this->stamm_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aStamm->addVersichertesObjekts($this);
             */
        }

        return $this->aStamm;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Beitrag' == $relationName) {
            $this->initBeitrags();
        }
    }

    /**
     * Clears out the collBeitrags collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return VersichertesObjekt The current object (for fluent API support)
     * @see        addBeitrags()
     */
    public function clearBeitrags()
    {
        $this->collBeitrags = null; // important to set this to null since that means it is uninitialized
        $this->collBeitragsPartial = null;

        return $this;
    }

    /**
     * reset is the collBeitrags collection loaded partially
     *
     * @return void
     */
    public function resetPartialBeitrags($v = true)
    {
        $this->collBeitragsPartial = $v;
    }

    /**
     * Initializes the collBeitrags collection.
     *
     * By default this just sets the collBeitrags collection to an empty array (like clearcollBeitrags());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBeitrags($overrideExisting = true)
    {
        if (null !== $this->collBeitrags && !$overrideExisting) {
            return;
        }
        $this->collBeitrags = new PropelObjectCollection();
        $this->collBeitrags->setModel('Beitrage');
    }

    /**
     * Gets an array of Beitrage objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this VersichertesObjekt is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Beitrage[] List of Beitrage objects
     * @throws PropelException
     */
    public function getBeitrags($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collBeitragsPartial && !$this->isNew();
        if (null === $this->collBeitrags || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBeitrags) {
                // return empty collection
                $this->initBeitrags();
            } else {
                $collBeitrags = BeitrageQuery::create(null, $criteria)
                    ->filterByVersichertesObjekt($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collBeitragsPartial && count($collBeitrags)) {
                      $this->initBeitrags(false);

                      foreach ($collBeitrags as $obj) {
                        if (false == $this->collBeitrags->contains($obj)) {
                          $this->collBeitrags->append($obj);
                        }
                      }

                      $this->collBeitragsPartial = true;
                    }

                    $collBeitrags->getInternalIterator()->rewind();

                    return $collBeitrags;
                }

                if ($partial && $this->collBeitrags) {
                    foreach ($this->collBeitrags as $obj) {
                        if ($obj->isNew()) {
                            $collBeitrags[] = $obj;
                        }
                    }
                }

                $this->collBeitrags = $collBeitrags;
                $this->collBeitragsPartial = false;
            }
        }

        return $this->collBeitrags;
    }

    /**
     * Sets a collection of Beitrag objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $beitrags A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return VersichertesObjekt The current object (for fluent API support)
     */
    public function setBeitrags(PropelCollection $beitrags, PropelPDO $con = null)
    {
        $beitragsToDelete = $this->getBeitrags(new Criteria(), $con)->diff($beitrags);


        $this->beitragsScheduledForDeletion = $beitragsToDelete;

        foreach ($beitragsToDelete as $beitragRemoved) {
            $beitragRemoved->setVersichertesObjekt(null);
        }

        $this->collBeitrags = null;
        foreach ($beitrags as $beitrag) {
            $this->addBeitrag($beitrag);
        }

        $this->collBeitrags = $beitrags;
        $this->collBeitragsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Beitrage objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Beitrage objects.
     * @throws PropelException
     */
    public function countBeitrags(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collBeitragsPartial && !$this->isNew();
        if (null === $this->collBeitrags || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBeitrags) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBeitrags());
            }
            $query = BeitrageQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByVersichertesObjekt($this)
                ->count($con);
        }

        return count($this->collBeitrags);
    }

    /**
     * Method called to associate a Beitrage object to this object
     * through the Beitrage foreign key attribute.
     *
     * @param    Beitrage $l Beitrage
     * @return VersichertesObjekt The current object (for fluent API support)
     */
    public function addBeitrag(Beitrage $l)
    {
        if ($this->collBeitrags === null) {
            $this->initBeitrags();
            $this->collBeitragsPartial = true;
        }

        if (!in_array($l, $this->collBeitrags->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddBeitrag($l);

            if ($this->beitragsScheduledForDeletion and $this->beitragsScheduledForDeletion->contains($l)) {
                $this->beitragsScheduledForDeletion->remove($this->beitragsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Beitrag $beitrag The beitrag object to add.
     */
    protected function doAddBeitrag($beitrag)
    {
        $this->collBeitrags[]= $beitrag;
        $beitrag->setVersichertesObjekt($this);
    }

    /**
     * @param	Beitrag $beitrag The beitrag object to remove.
     * @return VersichertesObjekt The current object (for fluent API support)
     */
    public function removeBeitrag($beitrag)
    {
        if ($this->getBeitrags()->contains($beitrag)) {
            $this->collBeitrags->remove($this->collBeitrags->search($beitrag));
            if (null === $this->beitragsScheduledForDeletion) {
                $this->beitragsScheduledForDeletion = clone $this->collBeitrags;
                $this->beitragsScheduledForDeletion->clear();
            }
            $this->beitragsScheduledForDeletion[]= clone $beitrag;
            $beitrag->setVersichertesObjekt(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->mod = null;
        $this->versicherungsnummer = null;
        $this->beschreibung = null;
        $this->kommentar = null;
        $this->stamm_id = null;
        $this->basispramie = null;
        $this->zahlweise = null;
        $this->iban = null;
        $this->bic = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collBeitrags) {
                foreach ($this->collBeitrags as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aStamm instanceof Persistent) {
              $this->aStamm->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collBeitrags instanceof PropelCollection) {
            $this->collBeitrags->clearIterator();
        }
        $this->collBeitrags = null;
        $this->aStamm = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(VersichertesObjektPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
