<?php
$this->nameMap = array (
	"Back" => "Zurück",
	"Class Selection" => "Typ",
	"Create" => "Anlegen",
	"List" => "Liste",
	"Select" => "Auswählen",
	"Submit changes" => "Änderungen speichern",
	"Kundigung" => "Kündigung zum",
);
